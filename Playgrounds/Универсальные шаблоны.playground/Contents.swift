/*:
 # Универсальные шаблоны (generics)
 
 ## Материалы для изучения
 
 [Универсальные шаблоны](https://swiftbook.ru/content/languageguide/generics-in-swift/)

 # Задача
 
 Имеется функция поиска индекса элемента в массиве строк.
 Реализовать схожую функцию, которая принимает массив элементов любого типа, с помощью универсальных шаблонов (generics)
*/

func findIndex(ofString valueToFind: String, in array: [String]) -> Int? {
    for (index, value) in array.enumerated() {
        if value == valueToFind {
            return index
        }
    }
    return nil
}
let strings = ["кот", "пес", "лама", "попугай", "черепаха"]
if let foundIndex = findIndex(ofString: "лама", in: strings) {
    print("Индекс ламы: \(foundIndex)")
}

let ints = [5, 7, 9, 12, -4, 42]
let doubles: [Double] = [0.2, 0.33, 0.5, 1.0, 1.5, 2.5, 3.0, 5.0, 10.0]



