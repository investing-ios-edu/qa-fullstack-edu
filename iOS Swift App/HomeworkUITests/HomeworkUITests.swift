//
//  HomeworkUITests.swift
//  HomeworkUITests
//
//  Created by a.sadomov on 09.09.2021.
//

import XCTest

class HomeworkUITests: XCTestCase {

    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    override func tearDownWithError() throws {
    }

    func testExample() throws {
        let app = XCUIApplication()
        app.launch()
        XCTAssert(app.staticTexts["Картошка"].waitForExistence(timeout: 10))
    }

}
