//
//  AddItemViewController.swift
//  Homework
//
//  Created by a.sadomov on 30.09.2021.
//

import UIKit

class AddItemViewController: UIViewController {

    @IBOutlet weak var descriptionField: UITextField!
    @IBOutlet weak var priceField: UITextField!
    
    var addItemHandler: ((String, Double) -> ShoppingListItem)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func addItem(_ sender: UIButton) {
        guard let text = self.descriptionField.text else { return }
        guard let price = self.priceField.text else { return }
        if let item = addItemHandler?(text, Double(price)!) {
            print("Товар \(item.name) по цене \(item.price) ₽ добавлен в список покупок!")
        }
        self.navigationController?.popViewController(animated: false)
    }
    
}
