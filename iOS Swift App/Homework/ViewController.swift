//
//  ViewController.swift
//  Homework
//
//  Created by a.sadomov on 09.09.2021.
//

import UIKit

enum ShoppingListItemType {
    case products
    case homeGoods
}

struct ShoppingListItem {
    var itemType: ShoppingListItemType
    var name: String
    var price: Double
    var isChecked = false
}

class ViewController: UIViewController {
    
    @IBOutlet weak var shoppingListTableview: UITableView!
    
    private var itemsData: [ShoppingListItem] = [
        ShoppingListItem(itemType: .products, name: "Картошка", price: 40.0),
        ShoppingListItem(itemType: .products, name: "Морковка", price: 25.0),
        ShoppingListItem(itemType: .products, name: "Кола", price: 103.0),
        ShoppingListItem(itemType: .products, name: "Хлеб", price: 19.80),
        ShoppingListItem(itemType: .products, name: "Масло", price: 109.9),
        ShoppingListItem(itemType: .products, name: "Молоко", price: 115.0),
        ShoppingListItem(itemType: .homeGoods, name: "Лампочка цоколь E27", price: 115.0),
        ShoppingListItem(itemType: .homeGoods, name: "Телевизор Xiaomi Mi TV A4", price: 24500.0)
    ]
    {
        didSet {
            self.shoppingListTableview.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? AddItemViewController {
            destinationViewController.addItemHandler = { (name, price) -> ShoppingListItem in
                let item = ShoppingListItem(itemType: .products, name: name, price: price)
                self.itemsData.append(item)
                return item
            }
        }
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath)
        let itemData = itemsData[indexPath.row]
        var content = UIListContentConfiguration.subtitleCell()
        switch itemData.itemType {
        case .products:
            content.image = UIImage(systemName: "cart")
        default:
            content.image = UIImage(systemName: "bag")
        }
        content.text = itemData.name
        content.secondaryText = "\(itemData.price) ₽"
        cell.contentConfiguration = content
        cell.accessoryType = itemsData[indexPath.row].isChecked ? .checkmark : .none
        return cell
    }
}


extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.itemsData[indexPath.row].isChecked = !self.itemsData[indexPath.row].isChecked
        tableView.reloadData()
    }
}

