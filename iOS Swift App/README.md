# Задание по UI-тестам под iOS

Перед вами простая реализация приложения "Список покупок" на языке **Swift**

Чтобы открыть приложение в **XCode** запустите файл **Homework.xcodeproj**

## Задача

Написать UI-тест, проверяющий добавление нового пункта в список покупок
